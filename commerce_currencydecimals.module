<?php

/**
 * For background @see https://drupal.org/node/1362002#comment-7929495
 */

/**
 * Implements hook_commerce_currency_info_alter().
 */
function commerce_currencydecimals_commerce_currency_info_alter(&$info) {
  $storage_offset = variable_get('commerce_currencydecimals_storage_offset', 0);
  $display_offset = variable_get('commerce_currencydecimals_display_offset', 0);
  if ($storage_offset || $display_offset) {
    foreach ($info as &$currency) {
      $currency += array('decimals' => 2);
      $decimals = $currency['decimals'];
      $currency['decimals'] = $decimals + $storage_offset;
      // Install our custom formatter.
      $currency['display_decimals'] = $decimals + $display_offset;
      $currency['format_callback'] = 'commerce_currencydecimals_format_currency';
    }
  }
}
function commerce_currencydecimals_format_currency($amount, $currency, $object) {
  // The rest copied verbatim from @see commerce_currency_format().
  $price = number_format(commerce_currency_round(abs($amount), $currency), $currency['display_decimals'], $currency['decimal_separator'], $currency['thousands_separator']);
  // Establish the replacement values to format this price for its currency.
  $replacements = array(
    '@code_before' => $currency['code_placement'] == 'before' ? $currency['code'] : '',
    '@symbol_before' => $currency['symbol_placement'] == 'before' ? $currency['symbol'] : '',
    '@price' => $price,
    '@symbol_after' => $currency['symbol_placement'] == 'after' ? $currency['symbol'] : '',
    '@code_after' => $currency['code_placement'] == 'after' ? $currency['code'] : '',
    '@negative' => $amount < 0 ? '-' : '',
    '@symbol_spacer' => $currency['symbol_spacer'],
    '@code_spacer' => $currency['code_spacer'],
  );
  return trim(t('@code_before@code_spacer@negative@symbol_before@price@symbol_spacer@symbol_after@code_spacer@code_after', $replacements));
}

/**
 * Implements hook_form_FORM_ID_alter() for commerce_currency_settings_form.
 */
function commerce_currencydecimals_form_commerce_currency_settings_form_alter(&$form, $form_state) {
  $form['actions']['#weight'] = 50;
  $form['commerce_currencydecimals_storage_offset'] = array(
    '#title' => t('Decimal storage offset'),
    '#description' => t('Change the storage accuracy of prices of all currencies by this. E.g. a number of 2 will change EUR from 2 to 4 decimal places, JOD from 3 to 5.'),
    '#type' => 'textfield',
    '#element_validate' => array('element_validate_integer'),
    '#default_value' => variable_get('commerce_currencydecimals_storage_offset', 0),
  );
  $form['commerce_currencydecimals_display_offset'] = array(
    '#title' => t('Decimal display offset'),
    '#description' => t('Change the display accuracy of prices of all currencies by this. E.g. a number of -2 will change EUR from 2 to 0 decimal places, JOD from 3 to 1.'),
    '#type' => 'textfield',
    '#element_validate' => array('element_validate_integer'),
    '#default_value' => variable_get('commerce_currencydecimals_display_offset', 0),
  );
}